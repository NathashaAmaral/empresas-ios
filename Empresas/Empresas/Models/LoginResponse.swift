//
//  LoginResponse.swift
//  Empresas
//
//  Created by Nathasha on 23/09/21.
//

struct LoginResponse: Decodable {
    let success: Bool

    enum CodingKeys: String, CodingKey {
        case success
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success) ?? false
    }
}
