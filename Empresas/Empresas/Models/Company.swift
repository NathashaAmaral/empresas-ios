//
//  Company.swift
//  Empresas
//
//  Created by Nathasha on 21/09/21.
//

import UIKit

struct EnterpriseType: Decodable {
    let name: String

    enum CodingKeys: String, CodingKey {
        case name = "enterprise_type_name"
    }
}

struct Company: Decodable {
    let companyName: String
    var companyImage: UIImage?
    let description: String
    let category: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case companyName = "enterprise_name"
        case companyImage = "photo"
        case description
        case category = "enterprise_type"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.companyName = try container.decodeIfPresent(String.self, forKey: .companyName) ?? ""
        let imageUrl = Api.shared.host + (try container.decodeIfPresent(String.self, forKey: .companyImage) ?? "")
        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        self.category = try container.decodeIfPresent(EnterpriseType.self, forKey: .category) ?? EnterpriseType(name: "")
        guard let url = URL(string: imageUrl) else {
            return
        }
        if let data = try? Data(contentsOf: url) {
            companyImage = UIImage(data: data)
        }
    }
}
