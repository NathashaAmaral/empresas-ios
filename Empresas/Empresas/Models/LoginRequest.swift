//
//  LoginRequest.swift
//  Empresas
//
//  Created by Nathasha on 23/09/21.
//

struct LoginRequest: Encodable {
    let email: String
    let password: String
}
