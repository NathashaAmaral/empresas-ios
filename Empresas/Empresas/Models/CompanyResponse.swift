//
//  CompanyResponse.swift
//  Empresas
//
//  Created by Nathasha on 23/09/21.
//

struct CompanyResponse: Decodable {
    let enterprises: [Company]?
}
