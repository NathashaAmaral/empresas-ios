//
//  FontExtension.swift
//  Empresas
//
//  Created by Nathasha on 09/09/21.
//

import UIKit

extension UIFont {
    static func gilroyBold(size: CGFloat) -> UIFont? {
        return UIFont(name: "Gilroy-Bold", size: size)
    }

    static func gilroyRegular(size: CGFloat) -> UIFont? {
        return UIFont(name: "Gilroy-Regular", size: size)
    }

    static func gilroyLight(size: CGFloat) -> UIFont? {
        return UIFont(name: "Gilroy-Light", size: size)
    }

    static func heeboMedium(size: CGFloat) -> UIFont? {
        return UIFont(name: "Heebo-Medium", size: size)
    }

    static func heeboLight(size: CGFloat) -> UIFont? {
        return UIFont(name: "Heebo-Light", size: size)
    }

    static func robotoLight(size: CGFloat) -> UIFont? {
        return UIFont(name: "Roboto-Light", size: size)
    }

    static func RobotoMedium(size: CGFloat) -> UIFont? {
        return UIFont(name: "Roboto-Medium", size: size)
    }
}


