//
//  HomeView.swift
//  Empresas
//
//  Created by Nathasha on 03/09/21.
//

import UIKit
import SwiftUI

final class HomeView: UIView {

    // MARK: - Views
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Pesquise por uma empresa"
        label.numberOfLines = 0
        label.font = .gilroyBold(size: 40)
        return label
    }()

    lazy var searchBar: SearchCustomTextField = {
        let textField = SearchCustomTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    lazy var lostImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "LostImage")
        view.contentMode = .scaleAspectFit
        return view
    }()

    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Empresa não encontrada"
        label.textColor = grayColor
        label.font = .heeboLight(size: 16)
        return label
    }()

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        return collectionView
    }()

    lazy var loadingView: LoadingView = {
        let view = LoadingView()
        return view
   }()

    // MARK: - Instaces
    private let grayColor = UIColor(red: 26/255, green: 27/255, blue: 28/255, alpha: 1)
    private var titleTopConstraint: NSLayoutConstraint?

    // MARK: - Inits
    init() {
        super.init(frame: .zero)
        backgroundColor = .white
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Methods
    func setupView() {
        addSubview(titleLabel)
        addSubview(searchBar)
        addSubview(lostImage)
        addSubview(messageLabel)
        addSubview(collectionView)
        addSubview(loadingView)

        titleTopConstraint =  titleLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 48)
        NSLayoutConstraint.activate([
            titleTopConstraint!,
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24)
        ])

        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 38),
            searchBar.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            searchBar.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            searchBar.heightAnchor.constraint(equalToConstant: 48)
        ])

        NSLayoutConstraint.activate([
            lostImage.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 80),
            lostImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 81),
            lostImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -81)
        ])

        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: lostImage.bottomAnchor, constant: 16),
            messageLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])

        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 32),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])

        NSLayoutConstraint.activate([
            loadingView.topAnchor.constraint(equalTo: topAnchor),
            loadingView.leadingAnchor.constraint(equalTo: leadingAnchor),
            loadingView.trailingAnchor.constraint(equalTo: trailingAnchor),
            loadingView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    func changeMainLabel(isSmallTitle: Bool) {
        UIView.animate(withDuration: 0.2) { [self] in
            titleLabel.textAlignment = isSmallTitle ? .center : .left
            titleTopConstraint?.constant = isSmallTitle ? 24 : 48
            titleLabel.text = isSmallTitle ? "Pesquise" : "Pesquise por uma empresa"
            let scale = isSmallTitle ? 0.6 : 1
            titleLabel.transform = CGAffineTransform(scaleX: scale, y: scale)
            layoutIfNeeded()
        }
    }
}
// MARK: SwiftUI Preview
#if DEBUG
struct HomeViewContainer: UIViewRepresentable {
typealias UIViewType = HomeView
func makeUIView(context: Context) -> UIViewType {
return HomeView()
    }
func updateUIView(_ uiView: HomeView, context: Context) {}
}
struct HomeViewContainer_Previews: PreviewProvider {
static var previews: some View {
        Group {
HomeViewContainer().colorScheme(.light)
HomeViewContainer().colorScheme(.dark)
        }.previewLayout(.fixed(width: 320, height: 700))
    }
}
#endif

