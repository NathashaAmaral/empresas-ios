//
//  HomeViewCompany.swift
//  Empresas
//
//  Created by Nathasha on 09/09/21.
//

import UIKit
import SwiftUI

final class HomeViewCompany: UIView {
    
    // MARK: - Views
    private lazy var backImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "Splash")
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()

    lazy var backButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "Back"), for: .normal)
        return button
    }()

    private lazy var companyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .gilroyBold(size: 24)
        return label
    }()

    private lazy var categoryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .gilroyRegular(size: 16)
        return label
    }()

    private lazy var companyImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.layer.cornerRadius = 5
        return view
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = grayColor
        label.font = .gilroyRegular(size: 16)
        return label
    }()

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        return view
    }()

    private var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: Instaces
    private let grayColor = UIColor(red: 147/255, green: 147/255, blue: 147/255, alpha: 1)

    // MARK: - Inits
    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods
    func set(company: Company) {
        companyNameLabel.text = company.companyName
        categoryLabel.text = company.category.name
        descriptionLabel.text = company.description
        companyImage.image = company.companyImage
    }

    func setupView() {
        backgroundColor = .white
        addSubview(backImage)
        addSubview(backButton)
        addSubview(companyNameLabel)
        addSubview(categoryLabel)
        scrollView.addSubview(stackView)
        addSubview(scrollView)
        stackView.addArrangedSubview(companyImage)
        stackView.addArrangedSubview(containerView)
        containerView.addSubview(descriptionLabel)

        NSLayoutConstraint.activate([
            backImage.topAnchor.constraint(equalTo: topAnchor),
            backImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            backImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            backImage.heightAnchor.constraint(equalToConstant: 148)
        ])

        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: topAnchor, constant: 73),
            backButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            backButton.widthAnchor.constraint(equalToConstant: 32)
        ])

        NSLayoutConstraint.activate([
            companyNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 72),
            companyNameLabel.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: 32),
            companyNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])

        NSLayoutConstraint.activate([
            categoryLabel.topAnchor.constraint(equalTo: companyNameLabel.bottomAnchor, constant: 4),
            categoryLabel.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: 32),
            categoryLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])

        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: backImage.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)
        ])

        NSLayoutConstraint.activate([
            companyImage.heightAnchor.constraint(equalToConstant: 244)
        ])

        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 31),
            descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 24),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -24),
            descriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])

    }
}
// MARK: SwiftUI Preview
#if DEBUG
struct HomeViewCompanyContainer: UIViewRepresentable {
typealias UIViewType = HomeViewCompany
func makeUIView(context: Context) -> UIViewType {
return HomeViewCompany()
    }
func updateUIView(_ uiView: HomeViewCompany, context: Context) {}
}
struct HomeViewCompanyContainer_Previews: PreviewProvider {
static var previews: some View {
        Group {
HomeViewCompanyContainer().colorScheme(.light)
HomeViewCompanyContainer().colorScheme(.dark)
        }.previewLayout(.fixed(width: 350, height: 700))
    }
}
#endif

