//
//  CompanyCellView.swift
//  Empresas
//
//  Created by Nathasha on 13/09/21.
//

import UIKit
import SwiftUI

class CompanyCellView: UICollectionViewCell {

    // MARK: - Views
    private lazy var logoView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.layer.cornerRadius = 16
        view.clipsToBounds = true
        return view
    }()

    private lazy var backView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "Splash")
        view.layer.cornerRadius = 16
        view.clipsToBounds = true
        return view
    }()

    private lazy var rectangleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        view.layer.shadowOpacity = 0.9
        view.layer.shadowRadius = 10
        view.layer.cornerRadius = 16
        view.layer.shadowColor = shadowColor.cgColor
        return view
    }()

    private lazy var nameCompany: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = grayColor
        label.font = .gilroyRegular(size: 14)
        label.textAlignment = .center
        return label
    }()


    // MARK: - Instaces
    private let grayColor = UIColor(red: 147/255, green: 147/255, blue: 147/255, alpha: 1)

    private let shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1)
    // MARK: - Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods
    func set(name: String, image: UIImage?) {
        nameCompany.text = name
        logoView.image = image
    }

    func setupView() {
        contentView.addSubview(backView)
        contentView.addSubview(logoView)
        contentView.addSubview(rectangleView)
        rectangleView.addSubview(nameCompany)
        backgroundColor = .white

        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: topAnchor, constant: 37),
            backView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ])

        NSLayoutConstraint.activate([
            logoView.topAnchor.constraint(equalTo: topAnchor),
            logoView.centerXAnchor.constraint(equalTo: centerXAnchor),
            logoView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            logoView.widthAnchor.constraint(equalToConstant: 88)
        ])

        NSLayoutConstraint.activate([
            rectangleView.bottomAnchor.constraint(equalTo: bottomAnchor),
            rectangleView.leadingAnchor.constraint(equalTo: leadingAnchor),
            rectangleView.trailingAnchor.constraint(equalTo: trailingAnchor),
            rectangleView.heightAnchor.constraint(equalToConstant: 33)
        ])

        NSLayoutConstraint.activate([
            nameCompany.topAnchor.constraint(equalTo: rectangleView.topAnchor, constant: 8),
            nameCompany.leadingAnchor.constraint(equalTo: rectangleView.leadingAnchor, constant: 8),
            nameCompany.trailingAnchor.constraint(equalTo: rectangleView.trailingAnchor, constant: -8),
            nameCompany.bottomAnchor.constraint(equalTo: rectangleView.bottomAnchor, constant: -8)
        ])
    }
}
// MARK: SwiftUI Preview
#if DEBUG
struct CompanyCellViewContainer: UIViewRepresentable {
    typealias UIViewType = CompanyCellView
    func makeUIView(context: Context) -> UIViewType {
        let cell = CompanyCellView()
        cell.backgroundColor = .white
        return cell
    }
    func updateUIView(_ uiView: CompanyCellView, context: Context) {}
}
struct CompanyCellViewContainer_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CompanyCellViewContainer().colorScheme(.light)
            CompanyCellViewContainer().colorScheme(.dark)
        }.previewLayout(.fixed(width: 152, height: 144))
    }
}
#endif
