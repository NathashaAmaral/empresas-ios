//
//  LoginView.swift
//  Empresas
//
//  Created by Nathasha on 31/08/21.
//

import UIKit
import SwiftUI

final class LoginView: UIView {

    // MARK: - Views
    private lazy var backGroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "Splash")
        return imageView
    }()

    private lazy var logoImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "LogoLogin")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var welcomeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Boas vindas,"
        label.font = .gilroyBold(size: 40)
        label.textColor = .white
        return label
    }()

    private lazy var companiesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Você está no Empresas."
        label.textColor = .white
        label.font = .heeboLight(size: 24)
        return label
    }()

    lazy var containerView: LoginContainerView = {
        let view = LoginContainerView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var loadingView: LoadingView = {
       let view = LoadingView()
       return view
   }()

    // MARK: - Inits
    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods
    private func setupView() {
        addSubview(backGroundImage)
        addSubview(logoImage)
        addSubview(welcomeLabel)
        addSubview(companiesLabel)
        addSubview(containerView)
        addSubview(loadingView)

        NSLayoutConstraint.activate([
            backGroundImage.topAnchor.constraint(equalTo: topAnchor),
            backGroundImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            backGroundImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            backGroundImage.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        NSLayoutConstraint.activate([
            logoImage.trailingAnchor.constraint(equalTo: backGroundImage.trailingAnchor, constant: screenWidth*0.25),
            logoImage.bottomAnchor.constraint(equalTo: containerView.topAnchor, constant: screenHeight*0.084),
            logoImage.widthAnchor.constraint(equalToConstant: screenWidth*0.95),
            logoImage.heightAnchor.constraint(equalToConstant: screenHeight*0.34)
        ])

        NSLayoutConstraint.activate([
            welcomeLabel.leadingAnchor.constraint(equalTo: backGroundImage.leadingAnchor, constant: 24),
            welcomeLabel.trailingAnchor.constraint(equalTo: backGroundImage.trailingAnchor, constant: -24)
        ])

        NSLayoutConstraint.activate([
            companiesLabel.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor),
            companiesLabel.leadingAnchor.constraint(equalTo: backGroundImage.leadingAnchor, constant: 24),
            companiesLabel.trailingAnchor.constraint(equalTo: backGroundImage.trailingAnchor, constant: -24)
        ])

        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: companiesLabel.bottomAnchor, constant: 31),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])

        NSLayoutConstraint.activate([
            loadingView.topAnchor.constraint(equalTo: topAnchor),
            loadingView.leadingAnchor.constraint(equalTo: leadingAnchor),
            loadingView.trailingAnchor.constraint(equalTo: trailingAnchor),
            loadingView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}

// MARK: SwiftUI Preview
#if DEBUG
struct LoginViewContainer: UIViewRepresentable {
typealias UIViewType = LoginView
func makeUIView(context: Context) -> UIViewType {
return LoginView()
    }
func updateUIView(_ uiView: LoginView, context: Context) {}
}
struct LoginViewContainer_Previews: PreviewProvider {
static var previews: some View {
        Group {
LoginViewContainer().colorScheme(.light)
LoginViewContainer().colorScheme(.dark)
        }.previewLayout(.fixed(width: 320, height: 568))
    }
}
#endif
