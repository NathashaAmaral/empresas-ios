//
//  LoginContainerView.swift
//  Empresas
//
//  Created by Nathasha on 31/08/21.
//

import UIKit
import SwiftUI

final class LoginContainerView: UIView {

    private lazy var diceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Digite seus dados para continuar."
        label.font = .gilroyBold(size: 16)
        label.textColor = .black
        return label
    }()

    lazy var loginTextField: CustomTextField = {
        let textField = CustomTextField(name: "Email")
        textField.textField.text = "testeapple@ioasys.com.br"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    lazy var passwordTextField: PasswordTextField = {
        let textField = PasswordTextField(name: "Senha")
        textField.textField.text = "12341234"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    lazy var buttonLogin: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = .heeboMedium(size: 14)
        let attributedText = NSMutableAttributedString(string: "ENTRAR", attributes: [.kern: 2])


        button.setAttributedTitle(attributedText, for: .init())
        button.backgroundColor = UIColor.gray
        button.layer.cornerRadius = 25
        button.setTitleColor(.white, for: .normal)
        return button
    }()

    // MARK: Instaces
    private let blackColor = UIColor(red: 39/255, green: 16/255, blue: 25/255, alpha: 1)

    var bottomConstraint: NSLayoutConstraint?

    init() {
        super.init(frame: .zero)
        backgroundColor = .white
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {

        addSubview(diceLabel)
        addSubview(loginTextField)
        addSubview(passwordTextField)
        addSubview(buttonLogin)

        NSLayoutConstraint.activate([
            diceLabel.topAnchor.constraint(equalTo: topAnchor, constant: 24),
            diceLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            diceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24)
        ])

        NSLayoutConstraint.activate([
            loginTextField.topAnchor.constraint(equalTo: diceLabel.bottomAnchor, constant: 32),
            loginTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            loginTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24)
        ])

        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: loginTextField.bottomAnchor, constant: 40),
            passwordTextField.leadingAnchor.constraint(equalTo: loginTextField.leadingAnchor),
            passwordTextField.trailingAnchor.constraint(equalTo: loginTextField.trailingAnchor)
        ])

        bottomConstraint =  buttonLogin.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        NSLayoutConstraint.activate([
            buttonLogin.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 42),
            buttonLogin.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor),
            buttonLogin.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor),
            bottomConstraint!,
            buttonLogin.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
}

// MARK: SwiftUI Preview
#if DEBUG
struct LoginContainerViewContainer: UIViewRepresentable {
typealias UIViewType = LoginContainerView
func makeUIView(context: Context) -> UIViewType {
return LoginContainerView()
    }
func updateUIView(_ uiView: LoginContainerView, context: Context) {}
}
struct LoginContainerViewContainer_Previews: PreviewProvider {
static var previews: some View {
        Group {
LoginContainerViewContainer().colorScheme(.light)
LoginContainerViewContainer().colorScheme(.dark)
        }.previewLayout(.fixed(width: 390, height: 500))
    }
}
#endif

