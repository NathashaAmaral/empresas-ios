//
//  API.swift
//  Empresas
//
//  Created by Nathasha on 23/09/21.
//

import Foundation

enum APPError: Error {
    case networkError(Error)
    case dataNotFound
    case jsonParsingError(Error)
    case invalidStatusCode(Int)
}

enum Result<T> {
    case success(T)
    case failure(APPError)
}

class Api {
    static let shared = Api()
    let host = "https://empresas.ioasys.com.br"
    private let apiVersion = "v1"
    
    func getCompanies(name: String, completion: @escaping (Result<CompanyResponse>) -> Void) {
        let dataURL = URL(string: "\(host)/api/\(apiVersion)/enterprises?name=\(name)")!

        let session = URLSession.shared

        var request = URLRequest(url: dataURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        let defaults = UserDefaults.standard

        let authorizationToken = defaults.string(forKey: "access-token") ?? ""
        let client = defaults.string(forKey: "client") ?? ""
        let uid = defaults.string(forKey: "uid") ?? ""

        request.setValue(authorizationToken, forHTTPHeaderField: "access-token")
        request.setValue(client, forHTTPHeaderField: "client")
        request.setValue(uid, forHTTPHeaderField: "uid")

        let task = session.dataTask(with: request, completionHandler: { data, response, error in

            guard error == nil else {
                completion(Result.failure(APPError.networkError(error!)))
                return
            }

            guard let data = data else {
                completion(Result.failure(APPError.dataNotFound))
                return
            }

            do {
                let decodedObject = try JSONDecoder().decode(CompanyResponse.self, from: data)
                completion(Result.success(decodedObject))
            } catch let error {
                //                swiftlint:disable force_cast
                completion(Result.failure(APPError.jsonParsingError(error as! DecodingError)))
            }
        })

        task.resume()
    }

    func loginRequest(body: LoginRequest, completion: @escaping (Result<LoginResponse>) -> Void) {
        let dataURL = URL(string: "\(host)/api/\(apiVersion)/users/auth/sign_in")!

        let session = URLSession.shared

        var request = URLRequest(url: dataURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)

        request.httpMethod = "POST"
        guard let jsonData = try? JSONEncoder().encode(body) else {
            print("Error: Trying to convert model to JSON data")
            return
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = jsonData

        let task = session.dataTask(with: request, completionHandler: { data, response, error in

            guard error == nil else {
                completion(Result.failure(APPError.networkError(error!)))
                return
            }

            guard let data = data else {
                completion(Result.failure(APPError.dataNotFound))
                return
            }

            do {
                if let httpResponse = response as? HTTPURLResponse {
                    if let auth = httpResponse.value(forHTTPHeaderField: "access-token"),
                       let client = httpResponse.value(forHTTPHeaderField: "client"),
                       let uid = httpResponse.value(forHTTPHeaderField: "uid") {
                        let defaults = UserDefaults.standard
                        defaults.set(auth, forKey: "access-token")
                        defaults.set(client, forKey: "client")
                        defaults.set(uid, forKey: "uid")
                    }
                }
                let decodedObject = try JSONDecoder().decode(LoginResponse.self, from: data)
                completion(Result.success(decodedObject))
            } catch let error {
                //                swiftlint:disable force_cast
                completion(Result.failure(APPError.jsonParsingError(error as! DecodingError)))
            }
        })

        task.resume()
    }
}

