//
//  SearchCustomTextField.swift
//  Empresas
//
//  Created by Nathasha on 10/09/21.
//

import UIKit
import SwiftUI

class SearchCustomTextField: UIView {

    // MARK: - Views
    lazy var borderView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderWidth = 1
        view.layer.borderColor = borderColor.cgColor
        view.layer.cornerRadius = 8
        return view
    }()

    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = grayColor
        textField.font = .robotoLight(size: 16)
        textField.keyboardType = .webSearch
        return textField
    }()

    private lazy var searchImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "Search")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    // MARK: - Instaces
    private let grayColor = UIColor(red: 26/255, green: 27/255, blue: 28/255, alpha: 1)

    private let borderColor = UIColor(red: 196/255, green: 196/255, blue: 196/255, alpha: 1)

    private let placeColor = UIColor(red: 118/255, green: 118/255, blue: 119/255, alpha: 1)

    // MARK: Inits
    init() {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .clear
        let attributedString = NSAttributedString(string: "Buscar...", attributes: [.foregroundColor: placeColor])
        textField.attributedPlaceholder = attributedString
        addSubview(borderView)
        borderView.addSubview(textField)
        textField.leftView = searchImage
        textField.leftViewMode = .always

        NSLayoutConstraint.activate([
            borderView.topAnchor.constraint(equalTo: topAnchor),
            borderView.leadingAnchor.constraint(equalTo: leadingAnchor),
            borderView.trailingAnchor.constraint(equalTo: trailingAnchor),
            borderView.bottomAnchor.constraint(equalTo: bottomAnchor),
            borderView.heightAnchor.constraint(equalToConstant: 48)
        ])

        NSLayoutConstraint.activate([
            textField.topAnchor.constraint(equalTo: borderView.topAnchor),
            textField.leadingAnchor.constraint(equalTo: borderView.leadingAnchor, constant: 16),
            textField.trailingAnchor.constraint(equalTo: borderView.trailingAnchor, constant: -16),
            textField.bottomAnchor.constraint(equalTo: borderView.bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: SwiftUI Preview
#if DEBUG
struct SearchCustomTextFieldContainer: UIViewRepresentable {
typealias UIViewType = SearchCustomTextField
func makeUIView(context: Context) -> UIViewType {
    return SearchCustomTextField()
    }
func updateUIView(_ uiView: SearchCustomTextField, context: Context) {}
}
struct SearchCustomTextFieldContainer_Previews: PreviewProvider {
static var previews: some View {
        Group {
SearchCustomTextFieldContainer().colorScheme(.light)
SearchCustomTextFieldContainer().colorScheme(.dark)
        }.previewLayout(.fixed(width: 320, height: 700))
    }
}
#endif
