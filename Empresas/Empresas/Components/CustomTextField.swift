//
//  CustomTextField.swift
//  Empresas
//
//  Created by Nathasha on 31/08/21.
//

import UIKit
import SwiftUI

class CustomTextField: UIView {

    // MARK: Views
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .RobotoMedium(size: 12)
        label.textColor = descriptionColor
        label.alpha = 0
        return label
    }()

    lazy var borderView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderWidth = 1
        view.layer.borderColor = borderColor.cgColor
        view.layer.cornerRadius = 5
        return view
    }()

    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textColor = textFieldColor
        textField.autocapitalizationType = .none
        return textField
    }()

    lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = redColor
        label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        return label
    }()

    // MARK: Instaces
   private let redColor = UIColor(red: 189/255, green: 78/255, blue: 78/255, alpha: 1)

    private let descriptionColor = UIColor(red: 84/255, green: 87/255, blue: 89/255, alpha: 1)

    private let textFieldColor = UIColor(red: 26/255, green: 27/255, blue: 28/255, alpha: 1)

    private let borderColor = UIColor(red: 196/255, green: 196/255, blue: 196/255, alpha: 1)

    private let placeColor = UIColor(red: 118/255, green: 118/255, blue: 119/255, alpha: 1)

    // MARK: Inits
    init(name: String) {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        layer.cornerRadius = 6
        let attributedString = NSAttributedString(string: name, attributes: [.foregroundColor: placeColor])
        descriptionLabel.text = name
        textField.attributedPlaceholder = attributedString
        addSubview(descriptionLabel)
        addSubview(borderView)
        borderView.addSubview(textField)
        textField.addSubview(errorLabel)

        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: topAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            borderView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor),
            borderView.leadingAnchor.constraint(equalTo: leadingAnchor),
            borderView.trailingAnchor.constraint(equalTo: trailingAnchor),
            borderView.bottomAnchor.constraint(equalTo: bottomAnchor),
            borderView.heightAnchor.constraint(equalToConstant: 48)
        ])

        NSLayoutConstraint.activate([
            textField.topAnchor.constraint(equalTo: borderView.topAnchor),
            textField.leadingAnchor.constraint(equalTo: borderView.leadingAnchor, constant: 16),
            textField.trailingAnchor.constraint(equalTo: borderView.trailingAnchor, constant: -16),
            textField.bottomAnchor.constraint(equalTo: borderView.bottomAnchor)
        ])

        NSLayoutConstraint.activate([
            errorLabel.topAnchor.constraint(equalTo: borderView.bottomAnchor),
            errorLabel.leadingAnchor.constraint(equalTo: borderView.leadingAnchor, constant: 8),
            errorLabel.trailingAnchor.constraint(equalTo: borderView.trailingAnchor)

        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Methods
    func set(error: String?) {
        let hasError = error != nil
            errorLabel.text = error
        descriptionLabel.textColor = hasError ? redColor : descriptionColor
        textField.textColor = hasError ? redColor : textFieldColor
        borderView.layer.borderColor = hasError ? redColor.cgColor : borderColor.cgColor
    }
}
// MARK: SwiftUI Preview
#if DEBUG
struct CustomTextFieldContainer: UIViewRepresentable {
    typealias UIViewType = CustomTextField
    func makeUIView(context: Context) -> UIViewType {
        let textField = CustomTextField(name: "email")
        textField.widthAnchor.constraint(equalToConstant: 327).isActive = true
        return textField
    }
    func updateUIView(_ uiView: CustomTextField, context: Context) {}
}
struct CustomTextFieldContainer_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CustomTextFieldContainer().colorScheme(.light)
            CustomTextFieldContainer().colorScheme(.dark)
        }.previewLayout(.fixed(width: 327, height: 200))
    }
}
#endif
