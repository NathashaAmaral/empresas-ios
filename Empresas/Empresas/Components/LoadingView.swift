//
//  LoadingView.swift
//  Empresas
//
//  Created by Nathasha on 07/10/21.
//

import UIKit

class LoadingView: UIView {
    lazy var loadingIndicator: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "LoadingAction")
        return view
    }()

    init() {
        super.init(frame: .zero)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func angle(degrees: CGFloat) -> CGFloat {
        return degrees * CGFloat(Double.pi)/180
    }

    private func loadingAnimate() {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.fromValue = angle(degrees: 360)
        animation.toValue = angle(degrees: 0)
        animation.duration = 2
        animation.repeatCount = .infinity
        loadingIndicator.layer.add(animation, forKey: "animation")
    }

    func set(isAnimating: Bool) {
        if isAnimating {
            loadingAnimate()
        }
        isHidden = !isAnimating
    }

    func setupUI() {
        addSubview(loadingIndicator)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        isHidden = true
        
        NSLayoutConstraint.activate([
            loadingIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            loadingIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}


