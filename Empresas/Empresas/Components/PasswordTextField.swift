//
//  PasswordTextField.swift
//  Empresas
//
//  Created by Nathasha on 31/08/21.
//

import UIKit
import SwiftUI

final class PasswordTextField: CustomTextField {

    // MARK: - Views
    lazy var eyeButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "Eye"), for: .normal)
        return button
    }()

    // MARK: - Inits
    override init(name: String) {
        super.init(name: name)
        setupUI()
        textField.isSecureTextEntry = true
    }
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        textField.rightView = eyeButton
        textField.rightViewMode = .always
    }
}
// MARK: SwiftUI Preview
#if DEBUG
struct PasswordTextFieldContainer: UIViewRepresentable {
typealias UIViewType = PasswordTextField
func makeUIView(context: Context) -> UIViewType {
    return PasswordTextField(name: "senha")
    }
func updateUIView(_ uiView: PasswordTextField, context: Context) {}
}
struct PasswordTextFieldContainer_Previews: PreviewProvider {
static var previews: some View {
        Group {
PasswordTextFieldContainer().colorScheme(.light)
PasswordTextFieldContainer().colorScheme(.dark)
        }.previewLayout(.fixed(width: 327, height: 48))
    }
}
#endif
