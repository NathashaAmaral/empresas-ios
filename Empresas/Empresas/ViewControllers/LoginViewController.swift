//
//  LoginViewController.swift
//  Empresas
//
//  Created by Nathasha on 31/08/21.
//

import UIKit

final class LoginViewController: UIViewController {

    private lazy var mainView = LoginView()
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = mainView
        mainView.containerView.passwordTextField.eyeButton.addTarget(self, action: #selector(securityAction), for: .touchUpInside)
        mainView.containerView.buttonLogin.addTarget(self, action: #selector(nextPagesAction), for: .touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.textFieldBeginEdit), name: UITextField.textDidBeginEditingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.textFieldEndEdit), name: UITextField.textDidEndEditingNotification, object: nil)
        hideKeyboardWhenTappedAround()
    }

    override func viewDidAppear(_ animated: Bool) {
        animate(constant: -74)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        animateWithKeyboard(notification: notification) { rect in
            self.mainView.containerView.bottomConstraint?.constant = -24 - rect.height
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        animateWithKeyboard(notification: notification) { rect in
            self.mainView.containerView.bottomConstraint?.constant = -74
        }
    }

    @objc func textFieldBeginEdit(textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [unowned self] in
            if mainView.containerView.loginTextField.textField.isFirstResponder {
                mainView.containerView.loginTextField.descriptionLabel.alpha = 1
            } else {
                mainView.containerView.passwordTextField.descriptionLabel.alpha = 1
            }
        }
        self.mainView.containerView.loginTextField.set(error: nil)
        self.mainView.containerView.passwordTextField.set(error: nil)
    }

    @objc func textFieldEndEdit(notification: Notification) {
        guard let textField = notification.object as? UITextField,
              let text = textField.text,
              text.isEmpty else {
                  return
              }
        UIView.animate(withDuration: 0.3) { [unowned self] in
            if mainView.containerView.loginTextField.textField == textField {
                mainView.containerView.loginTextField.descriptionLabel.alpha = 0
            } else {
                mainView.containerView.passwordTextField.descriptionLabel.alpha = 0
            }
        }
    }
    
    @objc func securityAction() {
        self.mainView.containerView.passwordTextField.textField.isSecureTextEntry.toggle()

    }

    @objc func nextPagesAction() {
        self.mainView.loadingView.set(isAnimating: true)
        let loginRequest = LoginRequest(email: self.mainView.containerView.loginTextField.textField.text ?? "" ,
                                        password: self.mainView.containerView.passwordTextField.textField.text ?? "")
        Api.shared.loginRequest(body: loginRequest) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.mainView.loadingView.set(isAnimating: false)
                switch result {
                case .success(let response):
                    if !response.success {
                        self.showAlert()
                    } else {
                        let viewController = HomeViewController()
                        viewController.modalPresentationStyle = .fullScreen
                        self.present(viewController, animated: true, completion: nil)
                    }
                case .failure:
                    self.showAlert()
                }
            }
        }
    }

    private func showAlert() {
        let refreshAlert = UIAlertController(title: "Erro", message: "E-mail ou Senha incorretos",
                                             preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        }))
        self.present(refreshAlert, animated: true, completion: nil)
        self.mainView.containerView.loginTextField.set(error: "Endereço de email inválido")
        self.mainView.containerView.passwordTextField.set(error: "Senha inválida")
    }

    private func animate(constant: CGFloat) {
        UIView.animate(withDuration: 1, animations: {
            self.mainView.containerView.bottomConstraint?.constant = constant
            self.mainView.layoutIfNeeded()
        })
    }

    func animateWithKeyboard(
        notification: NSNotification,
        animations: ((_ keyboardFrame: CGRect) -> Void)?
    ) {
        let durationKey = UIResponder.keyboardAnimationDurationUserInfoKey
        let duration = notification.userInfo![durationKey] as! Double
        let frameKey = UIResponder.keyboardFrameEndUserInfoKey
        let keyboardFrameValue = notification.userInfo![frameKey] as! NSValue

        let curveKey = UIResponder.keyboardAnimationCurveUserInfoKey
        let curveValue = notification.userInfo![curveKey] as! Int
        let curve = UIView.AnimationCurve(rawValue: curveValue)!

        let animator = UIViewPropertyAnimator(
            duration: duration,
            curve: curve
        ) {
            animations?(keyboardFrameValue.cgRectValue)

            self.view?.layoutIfNeeded()
        }

        animator.startAnimation()
    }
}
