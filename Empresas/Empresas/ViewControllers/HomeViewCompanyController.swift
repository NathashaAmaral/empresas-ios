//
//  HomeViewCompanyController.swift
//  Empresas
//
//  Created by Nathasha on 20/09/21.
//

import UIKit

final class HomeViewCompanyController: UIViewController {
    private lazy var mainView = HomeViewCompany()
    var didClose: (() -> Void)?
    override func loadView() {
        view = mainView
    }
    override func viewDidLoad() {
        mainView.backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }

    func set(company: Company) {
        mainView.set(company: company)
    }

    @objc func backAction() {
        didClose?()
        self.dismiss(animated: true, completion: nil)
    }
}
