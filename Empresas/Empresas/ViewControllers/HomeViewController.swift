//
//  HomeViewController.swift
//  Empresas
//
//  Created by Nathasha on 03/09/21.
//

import UIKit

final class HomeViewController: UIViewController {

    // MARK: - Instances
    private lazy var mainView = HomeView()
    var companiesArray: [Company] = []
    private let purpleColor = UIColor(red: 153/255, green: 75/255, blue: 189/255, alpha: 1)
    private let borderColor = UIColor(red: 196/255, green: 196/255, blue: 196/255, alpha: 1)

    // MARK: - Life Cycle
    override func loadView() {
        view = mainView
    }

    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.textFieldBeginEdit), name: UITextField.textDidBeginEditingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.textFieldEndEdit), name: UITextField.textDidEndEditingNotification, object: nil)
        hideKeyboardWhenTappedAround()
        togglePlaceholder()
        mainView.collectionView.delegate = self
        mainView.collectionView.dataSource = self
        mainView.collectionView.register(CompanyCellView.self, forCellWithReuseIdentifier: "CompanyCellView")
        mainView.searchBar.textField.delegate = self
    }

    // MARK: - Methods
    private func togglePlaceholder() {
        if companiesArray.isEmpty {
            self.mainView.lostImage.isHidden = false
            self.mainView.messageLabel.isHidden = false
        } else {
            self.mainView.lostImage.isHidden = true
            self.mainView.messageLabel.isHidden = true
        }
    }

    private func getCompanies() {
        self.mainView.loadingView.set(isAnimating: true)
        Api.shared.getCompanies(name: mainView.searchBar.textField.text ?? "") { [weak self] result in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self.companiesArray = response.enterprises ?? []
                    self.togglePlaceholder()
                    self.mainView.collectionView.reloadData()
                case .failure:
                    self.showAlert()
                }
                self.mainView.loadingView.set(isAnimating: false)
            }
        }
    }

    private func showAlert() {
        let refreshAlert = UIAlertController(title: "Erro", message: "Erro ao buscar a empresa",
                                             preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        }))
        self.present(refreshAlert, animated: true, completion: nil)
    }

    @objc private func textFieldBeginEdit(textField: UITextField) {
        mainView.searchBar.borderView.layer.borderColor = purpleColor.cgColor
            self.mainView.changeMainLabel(isSmallTitle: true)
    }

    @objc private func textFieldEndEdit(textField: UITextField) {
        mainView.searchBar.borderView.layer.borderColor = borderColor.cgColor
        guard let searchText = mainView.searchBar.textField.text else { return }
        if searchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.mainView.changeMainLabel(isSmallTitle: false)
        }
    }
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        getCompanies()
        mainView.endEditing(true)
        return true
    }
}

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = HomeViewCompanyController()
        let company = companiesArray[indexPath.row]
        viewController.set(company: company)
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.companiesArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CompanyCellView", for: indexPath) as? CompanyCellView {
            let company = companiesArray[indexPath.row]
            cell.set(name: company.companyName, image: company.companyImage)
            return cell
        }
        return UICollectionViewCell()
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width*0.46, height: 144)
    }
}
